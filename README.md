# Compose Apps Sharing a Wireguard Network

This is a proof of concept to be applied to useful services. The general idea is to combine two docker-compose apps: frontend and backend inside one virtual network such that both sides can see each other, all traffic is encrypted and with as little overhead as possible (just normal additional services on top of monolithic devnet profiles).

We use a simple heartbeat HTTP server as our sample backend app. It can be substituted for any service(s) and any port that Docker can normally forward to the host.

The goal of this architecture is to shield the backend services running on dedicated hardware and scaled vertically. Public access can be granted only via the frontend region running on commodity VPSs and scaled horizontally. Here, we test the simplest configuration (just one pair) but across regions. Regional sites can be added simply by running more peers. However, it is preferable for each regional site to have its own master to avoid a central point-of-failure. Sites should operate independently in a decentralized manner. 

We expect traffic to be shared between sites using a DNS load balancer ideally from a service provider like Azure. The right load balancer should have the ability to: a) Route by region; b) Close for business when the backend of a site does not respond to a heartbeat. CDNs and other such services **must be avoided** in favor of nginx and other services from this fleet: https://fleet.linuxserver.io/. It is critical for us to be able to trace the route of each request.

If this seems complicated, consider most services are included demonstrate what the app does. The only core services are the wg server, wg peer clients and the FTP server/client that fans out the generated peer config. Wireguard is a kernel module that ships commodity VPSs like DigitalOcean and Vultr. The Docker image completes the package by making the inclusion of peers as simple as downloading a file, hence the FTP channel.

## Prepare

Buy three separate VPSs. In this example, we run `master`on one DigitalOcean VPS in San Francisco. We run `backend` on a residential workstation in the same region with all inbound ports firewalled from the Internet. We run `frontend` on a Vultr VPS in Tokyo. We use Ubuntu 20.04 for both. Don't try collocate or you will experience port conflicts.

Checkout this repo on each VPS and then install the prerequisites:

- Install Docker (exact instructions tested): https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04
- Install the Compose plugin ("Install on Linux" exact instructions tested): https://docs.docker.com/compose/cli-command/
- Skip the `compose-switch` legacy tooling.

Total install time on first attempt: 11 minutes (overall for two servers)

NOTE - These are exact instructions executed on bran new servers. If you find yourself installing Wireguard modules or anything else, you bought the wrong VPS.

NOTE - `docker-compose` is now known as `docker compose`.

## Run

We include `.env` files for convenience at the moment. The **only** config required is to set the `MASTER_IP` corresponding to the VPS running the master profile (Wireguard server). Everything else is automated. The only other (optional) config would be the host firewall to restrict access. If you need to configure more things, something might be wrong with the previous steps. 

### Master

Annotated `.env` file in the `master` dir.

``` shell
# The pulic IP address of the VPS this run on.
MASTER_IP="x.x.x.x"

# The number of peer Wireguard config to generate.
# We create 3 extra peer configs for later use in this example.
PEERS=5

TZ="UTC"
```

``` shell
root@sf:/usr/local/src/wg-sites/master# ./compose-master.bash up --build
```
Look for a list of peer config files copied to the ftp server.

### Backend

Annotated `.env` file in the `site0` dir (the same file applies to backend and frontend).

``` shell
# The pulic IP address of the VPS this run on.
MASTER_IP="x.x.x.x"

# Frontend VPS info
# Internal VPN ip address can be derived from the peer Id (see PEERS created by master).
FRONTEND_PEER_ID=1
# The country code where the VPS is located.
FRONTEND_HOST_COUNTRY="JP"

BACKEND_PEER_ID=2
BACKEND_HOST_COUNTRY="US"

# Synchronize timezone with master.
TZ="UTC"
```

``` shell
root@sf:/usr/local/src/wg-sites/site0# ./compose-backend.bash up --build
```

Look for successful wg client and api logs.

### Frontend

``` shell
root@tokyo:/usr/local/src/wg-sites/site0# ./compose-frontend.bash up --build
```

Look at the persistent status report verifying the IP, country (expect US in our case) and sending heartbeats to the API server. Look at
the `status.bash` script for more details. Successful heartbeats prove that services are visible from within the VPN across the VPS border.

Example status report entry:

``` shell
site0-status-1   | started 2021-12-29 22:36:15 // updated 2021-12-29 22:36:15
site0-status-1   |
site0-status-1   | Current IP address (for information):
site0-status-1   | ╺┓ ┏━┓┏━┓   ┏━┓╺┓ ┏━┓
site0-status-1   |  ┃ ╺━┫┣━┫   ┏━┛ ┃ ╺━┫
site0-status-1   | ╺┻╸┗━┛┗━┛╹╸╹┗━╸╺┻╸┗━┛
site0-status-1   | Country is as expected:
site0-status-1   | ┌──────┐
site0-status-1   | │╻ ╻┏━┓│
site0-status-1   | │┃ ┃┗━┓│
site0-status-1   | │┗━┛┗━┛│
site0-status-1   | └──────┘
site0-status-1   | Sending heartbeat http://10.13.13.3:8000/
site0-status-1   | Heartbeat Successful
site0-status-1   | ┌────────────┐
site0-status-1   | │┏━┓┏━┓┏┓╻┏━╸│
site0-status-1   | │┣━┛┃ ┃┃┗┫┃╺┓│
site0-status-1   | │╹  ┗━┛╹ ╹┗━┛│
site0-status-1   | └────────────┘
site0-status-1   |
site0-status-1   | Next-hop IP address is as expected:
site0-status-1   | ╺┓ ┏━┓ ╺┓ ┏━┓ ╺┓ ┏━┓ ╺┓
site0-status-1   |  ┃ ┃┃┃  ┃ ╺━┫  ┃ ╺━┫  ┃
site0-status-1   | ╺┻╸┗━┛╹╺┻╸┗━┛╹╺┻╸┗━┛╹╺┻╸
site0-status-1   |
site0-status-1   | Available interfaces:
site0-status-1   | lo	inet	127.0.0.1/8
site0-status-1   | wg0	inet	10.13.13.2/32
site0-status-1   | eth0	inet	192.168.32.2/20
site0-status-1   |
site0-status-1   | Next hops per interface (only wg* should be permitted):
site0-status-1   | eth0	 1  192.168.32.1  0.151 ms  0.014 ms  0.014 ms
site0-status-1   | wg0	 1  10.13.13.1  108.414 ms  108.374 ms  108.349 ms
```

### Monitor Master

Example `wg` command:

``` shell
root@eddc00ddb07c:/# wg show
interface: wg0
  public key: WerG1ZwGHe2s+qv0DbzbkcoO6UNDcQfrtlZP+ADO9yA=
  private key: (hidden)
  listening port: 51820

#####FRONTEND
peer: sUOpPzaYmzAYgh3STNMrIfnysDltcSbMVndeFBGaeyQ=
  endpoint: x.x.x.x:51820
  allowed ips: 10.13.13.2/32
  latest handshake: 10 seconds ago
  transfer: 202.62 KiB received, 394.76 KiB sent

#####BACKEND
peer: N7rASnO+9rLUY/rr3z5EaSdL03XmIsXF+MRQ15sJI2M=
  endpoint: y.y.y.y:47937
  allowed ips: 10.13.13.3/32
  latest handshake: 1 minute, 10 seconds ago
  transfer: 84.36 KiB received, 2.60 MiB sent

peer: MaALnqSmvpW8Tl6u0nEmhUz1BUajFZjgiXc3EdWO/mc=
  allowed ips: 10.13.13.4/32

peer: kz7YHE/S0iVwa0Uw4BMaaq0EPzrtBFf6y5jt7oEbShk=
  allowed ips: 10.13.13.5/32

peer: BS/OGzUXOoAi3M9+BHiwz62U7c3XlDfitTjUtgRotxM=
  allowed ips: 10.13.13.6/32
```

### Public API

We added a nginx server to the frontend profile for good measure. Use it to confirm that the SF backend service is only exposed in Tokyo: `http://tokyo-vps-ip/ping/`.

``` shell
site0-nginx-1    | 2021-12-30T01:32:22+00:00|200|cache -|http_x_forwarded_for:: -|remote_addr: 192.168.192.1|binary_remote_addr: \xC0\xA8\xC0\x01|upstream_addr: 10.13.13.3:8000|GET http://localhost/ping|14 bytes in 0.260ms|curl/7.68.0
```

## Docs

Read the comments in the compose files. Contributions appreciated.
