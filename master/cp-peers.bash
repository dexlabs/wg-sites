#!/bin/bash

set -e

echo "Waiting 3 seconds to ensure Wireguard created the ${PEERS} peers"
sleep 3s

cd /config

for PEER_ID in $(seq 1 ${PEERS}); do
    echo "Copying peer ${PEER_ID} config"
    cp -v /config/"peer${PEER_ID}"/"peer${PEER_ID}.conf" /var/ftp/
done

chmod 774 /var/ftp/*.conf
echo "Done"
ls -lh /var/ftp/
