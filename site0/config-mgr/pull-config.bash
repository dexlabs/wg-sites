#!/bin/bash
#
set -e

echo "Pulling Wireguard config from ${MASTER_IP}"

cd /tmp
lftp -e "get peer${PEER_ID}.conf;quit" ${MASTER_IP}

# Keeping the original for future reference.
cp "peer${PEER_ID}.conf" "wg0.conf"
echo "Found peer ${PEER_ID} wg0.conf"
echo "$(rg --passthru '^PrivateKey = .*$' -r '(hidden private key)' wg0.conf)"
echo

# NOTE: Add killswitch firewall rules to the config: https://manpages.debian.org/unstable/wireguard-tools/wg-quick.8.en.html
# One might attempt the so-called ``kill-switch'', in order to prevent the flow of
# unencrypted packets through the non-WireGuard interfaces, by adding the following two lines `PostUp` and `PreDown`
# lines to the `[Interface]` section:
#
# PostUp = iptables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
# PreDown = iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
#
# We do not sandbox the frontend to allow port forwarding. Alternatively, we could modify the rules to only internal outbound traffic.
#
# iptables -A INPUT -d "$ALLOWED" -j ACCEPT
# iptables -A OUTPUT -d "$ALLOWED" -j ACCEPT
#
if [[ -z "$NO_KILLSWITCH" ]]; then
    rg --passthru -F '[Interface]' -r $'[Interface]\nPostUp = iptables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT\nPreDown = iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT' wg0.conf > tmp.conf && mv tmp.conf wg0.conf
    echo "Added killswitch to wg0.conf"
    echo "$(rg --passthru '^PrivateKey = .*$' -r '(hidden private key)' wg0.conf)"
    echo
fi

cd /config
cp /tmp/wg0.conf ./
chmod 774 ./wg0.conf

# This pre-defined config file is all the configuration needed by the Wireguard client.
echo "Ready to start Wireguard"
