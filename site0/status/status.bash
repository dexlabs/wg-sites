#!/bin/bash

# Generate the status report as ANSI and HTML and text files.
#
# The ANSI form is also shown on stdout. The HTML form is stored on a volume,
# and is server by a separately running web server (in another container).
#
set -euo pipefail
shopt -s nocasematch
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
# Watch out for false negative if timeout is too short
export CURL_TIMEOUT="15"
# Annoyingly, ip 1 of the subnet is the master so we must add one to all peer ids.
IP_SUFFIX=$((${BACKEND_PEER_ID} + 1))
API_URL="http://10.13.13.${IP_SUFFIX}:8000/"

# Use the ipleak service to get info about our public IP.
: ${IP_INFO_URL:="ipleak.net/json/"}

# Which IP to ping/traceroute for checking the connection.
# Just google for "pingable ip".
: ${STATUS_IP:="139.130.4.5"}

# The DNS resolver that is un-blocked in the firewall.
: ${NS:="8.8.8.8"}

# Where the status files are stored before showing. Note: they are cached.
: ${STATUS_DIR:="/tmp"}
temp_file="${STATUS_DIR}/index.temp"
ansi_file="${STATUS_DIR}/index.ansi"

# ANSI code: can be either color/mode names, or purpose names (e.g. "title").
# Multiple codes can be combined, e.g. `echo hello | ansi red blink invert`.
declare -A ANSI=(
  [brightwhite]='0;97'
  [midgray]='0;37'
  [gray]='1;30'
  [red]='0;31'
  [green]='0;32'
  [yellow]='0;33'
  [bold]='1'
  [blink]='5'
  [invert]='7'
)

# Wrap every line of the input into ANSI code. Unlike the whole-block wrapping,
# per-line wrapping is needed to fit into docker-compose, which adds its own
# colorful prefixes for each container, and they break the block's ANSI codes.
# For example: both the container name and the country name are ANSI-coded,
# but the country name is multiline here, each line must be ANSI-coded the same:
#     status_1       | ┌─────────────────────┐
#     status_1       | │┏━╸┏━╸┏━┓┏┳┓┏━┓┏┓╻╻ ╻│
#     status_1       | │┃╺┓┣╸ ┣┳┛┃┃┃┣━┫┃┗┫┗┳┛│
#     status_1       | │┗━┛┗━╸╹┗╸╹ ╹╹ ╹╹ ╹ ╹ │
#     status_1       | └─────────────────────┘
function ansi() {
  local esc=$(printf '\033')
  local codes=""
  for code_or_name in "$@"; do
    codes="${codes};${ANSI[$code_or_name]:-$code_or_name}"
  done
  sed -e "s/^/${esc}[${codes}m/g" -e "s/\$/${esc}[0m/g"
}

# Stylise the text on the input with big letters and maybe a border.
function show() {
  toilet --width 100 -f future --filter crop "$@"
}

# A unified date-time format.
function now() {
  date +'%Y-%m-%d %H:%M:%S'
}

# Generate the ANSI output and write it to a file.
started=$(now)
{
  echo "started $started // updated $(now)" | ansi midgray
  echo

  # Detect the IP address of the traffic, i.e. how the internet sees us.
  IP_INFO=$(curl -s "${IP_INFO_URL}" --connect-timeout "${CURL_TIMEOUT}")
  myip=$(echo "$IP_INFO" | jq -r .ip)
  if [[ -z "$myip" ]]; then
    echo "Current IP address cannot be detected:" | ansi gray
    echo "?.?.?.?" | show | ansi yellow
  else
    echo "Current IP address (for information):" | ansi gray
    echo "${myip}" | show | ansi brightwhite
  fi

  # Parse country code.
  country=$(echo "$IP_INFO" | jq -r .country_code)
  if [[ -z "$country" || "$country" == null ]]; then
    echo "Country cannot be detected:" | ansi gray
    echo "-=-=-=-" | show --filter border | ansi yellow
  elif [[ "$country" == "$HOST_COUNTRY" ]]; then
    echo "Country must NOT be ${HOST_COUNTRY}" | ansi red
    echo "${country}" | show --filter border | ansi red blink invert
  else
    echo "Country is as expected:" | ansi gray
    echo "${country}" | show --filter border | ansi green
  fi

  echo "Sending heartbeat ${API_URL}" | ansi bold
  res=$(curl -s "${API_URL}" --connect-timeout "${CURL_TIMEOUT}")
  if [[ -z "$res" ]]; then
    echo "Missed heartbeat" | ansi red
  else
    echo "Heartbeat Successful" | ansi midgray
    echo "${res}" | show --filter border | ansi green
  fi
  echo

  # Detect the next-hop IP address with the default routing: is it VPN or a local network?
  nexthop=$(traceroute -n -m1 -q1 "${STATUS_IP}" 2>/dev/null | tail -n+2 | awk '{print $2}' || true)
  if [[ -z "${nexthop}" ]]; then
    echo "Next-hop IP address is absent (blocked):" | ansi gray
    echo "-*-*-*-" | show | ansi yellow
  elif [[ "${nexthop}" != "10."* ]]; then
    echo "Next-hop IP address must be 10.*.*.*:" | ansi red
    echo "${nexthop}" | show | ansi red
  else
    echo "Next-hop IP address is as expected:" | ansi gray
    echo "${nexthop}" | show | ansi green
  fi

  # Print some additional information about the networking setup.
  # It is better to alwats see it directly rather than interpreted.
  echo
  echo "Available interfaces:" | ansi bold
  ip -oneline -color a show | awk '{print $2 "\t" $3 "\t" $4}'

  echo
  echo "Next hops per interface (only wg* should be permitted):" | ansi bold
  ip -o a | awk '{print $2}' | sort -u | grep -v '^lo' | xargs -n1 -I {} bash -c 'echo -en "{}\t"; traceroute -n -m1 -q3 -i {} "'"${STATUS_IP}"'" 2>&1 | tail -n+2 || true'

  echo
} >"$temp_file" 2>&1 || true
mv -f "${temp_file}" "${ansi_file}"  # atomic switch
