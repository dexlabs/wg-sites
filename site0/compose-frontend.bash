#!/bin/bash

# NOTE - `docker compose` is the cmd formerly known as `docker-compose`.
docker compose -f docker-compose-frontend.yml -f docker-compose.yml --profile init "$@"
docker compose -f docker-compose-frontend.yml -f docker-compose.yml --profile vpn "$@"
